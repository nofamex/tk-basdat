from django.shortcuts import render, redirect
from .forms import CreateEventForm, UpdateEventForm
from .db import get_event_by_id, get_event_ticket_by_id, get_event_testimonials_by_id, get_all_event, get_tema_by_id
from django.db import connection

def table_event(request):
    """
    function untuk menampilkan tabel event.
    """
    data = get_all_event()
    try:
        user = request.session['user']
        context = {
            'userName' : user[0],
            'typeUser': user[1],
            'login' : True,
            'datas' : data
        }
        return render(request, 'event/read_event.html', context)
    except KeyError:
        pass
    
    context = {
        'datas' : data
    }


    return render(request, 'event/read_event.html', context)


def create_event(request):
    """
    function untuk membuat event.
    """

    form = CreateEventForm(request.POST or None)
    context = {
        'form': form,
        'error': []
    }

    return render(request, 'event/create_event.html', context)


def update_event(request, id):
    """
    function untuk memperbarui data event.
    """
    if 'user' not in request.session:
        return redirect('/login/')

    data = get_event_by_id(id)[0]
    tema = get_tema_by_id(id)

    tema_list = [nama for (_, nama) in tema]

    form = UpdateEventForm(request.POST or None, initial={
        'nama_event': data.nama,
        'desc_event': data.deskripsi,
        'tgl_mulai': data.tanggal_mulai,
        'tgl_selesai': data.tanggal_selesai,
        'jam_mulai': data.jam_mulai,
        'jam_selesai': data.jam_selesai,
        'tipe_event': data.id_tipe,
        'nama_gedung': data.nama_gedung,
        'kota': data.kota,
        'alamat': data.alamat,
        'tema_event': tema_list,
        'kapasitas': data.kapasitas_total_tersedia
    })
    context = {
        'form': form,
        'error': []
    }

    if request.method == "POST" and form.is_valid():
        print(request.POST)


    return render(request, 'event/update_event.html', context)


def delete_event(request):
    """
    function untuk menghapus sebuah data.
    """
    id_event = request.POST["id_event"]
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO event;")
        cursor.execute(
            f"""
            DELETE FROM event
            WHERE id_event = '{id_event}';
            """
        )

    return redirect('/event/table/')

def page_event(request, id):
    """
    function untuk melihat event per id
    """
    event = get_event_by_id(id)[0]
    tickets = get_event_ticket_by_id(id)
    testimonials = get_event_testimonials_by_id(id)

    avg_rating = 0
    for testi in testimonials:
        avg_rating += testi.rating
    avg_rating = avg_rating/len(testimonials)

    context = {
        'id': id,
        'event': event,
        'tickets': tickets,
        'testimonials': testimonials,
        'avg_rating': avg_rating
    }
    return render(request, 'event/page_event.html', context)
