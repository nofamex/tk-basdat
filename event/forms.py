from django import forms

class CreateEventForm(forms.Form):
    nama_event = forms.CharField(
        label='Nama Event',
        max_length=128,
    )
    desc_event = forms.CharField(
        label='Deskripsi Event',
        max_length=256,
    )
    tema_event = forms.CharField(
        label='Tema Event',
        widget=forms.RadioSelect(
            choices=[
                (1, 'Green'),
                (2, 'Party'),
                (3, 'Wedding Daze'),
                (4, 'Water'),
            ]
        )
    )
    tgl_mulai = forms.DateField(
        label='Tanggal Mulai (YYYY-MM-DD)',
        widget = forms.SelectDateWidget
    )
    tgl_selesai = forms.DateField(
        label='Tanggal Selesai (YYYY-MM-DD)',
        widget = forms.SelectDateWidget
    )
    jam_mulai = forms.TimeField(
        label='Jam Mulai'
    )
    jam_selesai = forms.TimeField(
        label='Jam Selesai'
    )
    tipe_event = forms.CharField(
        label='Tipe Event',
        widget=forms.RadioSelect(
            choices=[
                (1, 'Music'),
                (2, 'Technology'),
                (3, 'Gaming'),
                (4, 'Economy'),
            ]
        )
    )
    # Lokasi Event
    nama_gedung = forms.CharField(
        label='Nama Gedung',
        max_length=128,
    )
    kota = forms.CharField(
        label='Kota',
        max_length=128,
    )
    alamat = forms.CharField(
        label='Alamat',
        max_length=256,
    )
    # Daftar kelas Tiket
    nama_kelas = forms.CharField(
        label='Nama Kelas',
        max_length=64,
    )
    tarif = forms.IntegerField(
        label='Tarif',
    )
    kapasitas = forms.IntegerField(
        label='Kapasitas',
    )

class UpdateEventForm(forms.Form):
    nama_event = forms.CharField(
        label='Nama Event',
        max_length=128,
    )
    desc_event = forms.CharField(
        label='Deskripsi Event',
        max_length=256,
    )
    tema_event = forms.CharField(
        label='Tema Event',
        widget=forms.CheckboxSelectMultiple(
            choices=[
                ('Sunday','Sunday'),
                ('Boating','Boating'),
                ('Strange','Strange'),
                ('Oz','Oz'),
                ('Wedding','Wedding'),
                ('Green','Green'),
                ('Pillar','Pillar'),
                ('1945','1945'),
                ('the Devil','the Devil'),
                ('Walls','Walls'),
                ('Prisoner','Prisoner'),
                ('Ra.One','Ra.One'),
                ('Corner','Corner'),
('Shades',                    'Shades'),
('Wakanda',                   'Wakanda'),
('Zombeavers',                   'Zombeavers'),
('Invasion',                   'Invasion'),
('Water',                     'Water'),
('I Want You',                   'I Want You'),
('Hell Ride',                   'Hell Ride'),
('Tyson',                     'Tyson'),
('Detention',                   'Detention'),
('Secret',                    'Secret'),
('Tough',                     'Tough'),
('Vessels',                   'Vessels'),
('Last Hero',                   'Last Hero'), 
('Steal',                     'Steal'),
('Detroit',                   'Detroit'),
('Spanish',                   'Spanish'),
('Winnipeg',                   'Winnipeg'),
('Obsession',                   'Obsession'),
('Physic',                    'Physic'),
('Batman',                    'Batman'),
('Power',                     'Power'),
('Blues',                     'Blues'),
('Aberdeen',                  'Aberdeen'),
('Babylon',                   'Babylon'),
('T-Rex',                     'T-Rex'),
('Summer',                    'Summer'),
('August',                    'August'),
            ]
        )
    )
    tgl_mulai = forms.DateField(
        label='Tanggal Mulai (YYYY-MM-DD)',
        widget = forms.SelectDateWidget
    )
    tgl_selesai = forms.DateField(
        label='Tanggal Selesai (YYYY-MM-DD)',
        widget = forms.SelectDateWidget
    )
    jam_mulai = forms.TimeField(
        label='Jam Mulai'
    )
    jam_selesai = forms.TimeField(
        label='Jam Selesai'
    )
    tipe_event = forms.CharField(
        label='Tipe Event',
        widget=forms.RadioSelect(
            choices=[
                (1, 'Music'),
                (2, 'Technology'),
                (3, 'Gaming'),
                (4, 'Sport'),
                (5, 'Finance'),
                (6, 'Envi'),
                (7, 'Economy'),
                (8, 'Humanities'),
                (9, 'Philant'),
                (10, 'Philosophy'),
            ]
        )
    )
    # Lokasi Event
    nama_gedung = forms.CharField(
        label='Nama Gedung',
        max_length=128,
    )
    kota = forms.CharField(
        label='Kota',
        max_length=128,
    )
    alamat = forms.CharField(
        label='Alamat',
        max_length=256,
    )
