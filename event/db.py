from django.db import connection
from .util import namedtuplefetchall

def get_event_by_id(id):
    with connection.cursor() as cursor:
        cursor.execute("select e.*, t.nama_tipe, l.* from event.event e join event.tipe t on e.id_tipe = t.id_tipe join event.lokasi_penyelenggara_event lp on lp.id_event = e.id_event join event.lokasi l on l.kode = lp.kode_lokasi where e.id_event = %s", [id])
        rows = namedtuplefetchall(cursor)
    return rows

def get_event_ticket_by_id(id):
    with connection.cursor() as cursor:
        cursor.execute("select * from event.kelas_tiket where id_event = %s", [id])
        rows = namedtuplefetchall(cursor)
    return rows

def get_tema_by_id(id):
    with connection.cursor() as cursor:
        cursor.execute("select * from event.tema where id_event = %s", [id])
        rows = namedtuplefetchall(cursor)
    return rows

def get_event_testimonials_by_id(id):
    with connection.cursor() as cursor:
        cursor.execute("select t.* from event.daftar_tiket d join event.testimoni t on t.email_pemilik_tiket = d.email_pemilik_tiket and t.email_pengunjung = d.email_pengunjung and t.tanggal_transaksi = d.tanggal_transaksi and t.waktu_transaksi = d.waktu_transaksi where d.id_event = %s", [id])
        rows = namedtuplefetchall(cursor)
    return rows

def get_all_event():
    with connection.cursor() as cursor:
        cursor.execute("select distinct e.*, t.nama_tema, l.nama_gedung from event.event e, event.tema t, event.lokasi l, event.lokasi_penyelenggara_event lpe where e.id_event = t.id_event and lpe.id_event = e.id_event and lpe.kode_lokasi = l.kode")
        rows = namedtuplefetchall(cursor)
        return rows
