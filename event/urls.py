from django.urls import path, include
from . import views

urlpatterns = [
    path("table/", views.table_event, name='table_event'),
    path("create/", views.create_event, name='create_event'),
    path("table/delete/", views.delete_event),
    path("update/<str:id>", views.update_event, name='update_event'),
    path("<str:id>/", views.page_event, name='page_event'),
]
