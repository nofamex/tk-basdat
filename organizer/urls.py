from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.page_organizer, name='page_organizer'),
    path("events/", views.events_organizer, name='events_organizer'),
]
