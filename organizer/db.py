from django.db import connection
from .util import namedtuplefetchall

def get_organizer_by_id(id):
    with connection.cursor() as cursor:
        cursor.execute("select z.email, concat(z.nama_depan, ' ', z.nama_belakang) as nama from event.organizer o join event.individu z on z.email = o.email where o.email = %s", [id])
        rows = namedtuplefetchall(cursor)
        if rows != []:
            return rows

        cursor.execute("select z.email, z.nama from event.organizer o join event.perusahaan z on z.email = o.email where o.email = %s", [id])
        rows = namedtuplefetchall(cursor)
    return rows

def get_events_by_id(id):
    with connection.cursor() as cursor:
        cursor.execute("select e.*, t.nama_tipe, l.* from event.event e join event.tipe t on e.id_tipe = t.id_tipe join event.lokasi_penyelenggara_event lp on lp.id_event = e.id_event join event.lokasi l on l.kode = lp.kode_lokasi where e.email_organizer = %s", [id])
        rows = namedtuplefetchall(cursor)
    return rows

def get_testimonials_by_organizer_id(id):
    with connection.cursor() as cursor:
        cursor.execute("select count(t.*), avg(t.rating) from event.daftar_tiket d join event.testimoni t on t.email_pemilik_tiket = d.email_pemilik_tiket and t.email_pengunjung = d.email_pengunjung and t.tanggal_transaksi = d.tanggal_transaksi and t.waktu_transaksi = d.waktu_transaksi where d.id_event in (select id_event from event.event e where e.email_organizer = %s)", [id])
        rows = namedtuplefetchall(cursor)
    return rows
