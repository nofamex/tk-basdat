from django.shortcuts import render, redirect
from .db import get_organizer_by_id, get_events_by_id, get_testimonials_by_organizer_id

# Create your views here.
def page_organizer(request):
    """
    function untuk melihat organizer
    """
    email = request.GET.get('e') or None
    organizer = get_organizer_by_id(email)
    if (organizer == []): return redirect('table_event')

    events = get_events_by_id(email)
    testimonial = get_testimonials_by_organizer_id(email)[0]

    context = {
        'id': email,
        'organizer': organizer[0],
        'events': events,
        'testimonial': testimonial
    }
    return render(request, 'organizer/page_organizer.html', context)

def events_organizer(request):
    """
    function untuk melihat dan mengubah events milik organizer
    """
    user_data = {}
    if 'user' not in request.session:
        return redirect('/login')
    else:
        user_data = request.session['user']

    if user_data[1] != 'organizer':
        return redirect('/')

    events = get_events_by_id(user_data[0])
    print(events)
    context = {
        'events': events,
        'typeUser': user_data[1]
    }
    return render(request, 'organizer/events_organizer.html', context)
