/**
 * function untuk menghapus baris pada data event.
 * @param id_event Primary Key
 * @param token CSRF token
 */
function deleteRowEvent(id_event, token) {
    console.log(id_event)

    $.ajax({
        headers: { "X-CSRFToken": token },
        type: "POST",
        url: "/event/table/delete/",
        data: { id_event: id_event },
        success: function () {
            console.log("Data sukses di kirim ke Django");
        },
    });
}