from django.db import connection
from .util import namedtuplefetchall

def get_user(id,password):
    with connection.cursor() as cursor:
        cursor.execute("select email, password from event.pengguna where email = %s and password = %s",[id, password])
        rows = namedtuplefetchall(cursor)
        return rows

def get_organizer_email(id):
    with connection.cursor() as cursor:
        cursor.execute("select email from event.organizer where email = %s", [id])
        rows = namedtuplefetchall(cursor)
        return rows

def get_pengunjung_email(id):
    with connection.cursor() as cursor:
        cursor.execute("select email from event.pengunjung where email = %s", [id])
        rows = namedtuplefetchall(cursor)
        return rows