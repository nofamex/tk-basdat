from django.shortcuts import render, redirect
from .forms import LoginForm
from .db import get_organizer_email, get_pengunjung_email, get_user

def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = get_user(email, password)

            if user == []:
                context = {
                    'form': form,
                    "false" : True
                }
                return render(request, 'auth/login.html', context)
            
            organizer = get_organizer_email(user[0].email)
            pengunjung = get_pengunjung_email(user[0].email)
            
            if pengunjung != []:
                request.session['user'] = [email, 'pengunjung']
                return redirect('/event/table')

            if organizer != []:
                request.session['user'] = [email, 'organizer']
                return redirect('/event/table')

           
    else:
        form = LoginForm(request.POST or None)
        context = {
        'form': form
        }
        return render(request, 'auth/login.html', context)