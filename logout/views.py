from django.shortcuts import render, redirect

# Create your views here.
def user_logout(request):
    if request.method == 'POST':
        try:
            del request.session['user']
            return redirect('/')
        except KeyError:
            pass
    else:
        return render(request, 'auth/logout.html')

def back_home(request):
    return redirect('/')