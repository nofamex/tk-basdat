from django.db import connection
from .util import namedtuplefetchall

def get_user(id,password):
    with connection.cursor() as cursor:
        cursor.execute("select email, password from event.pengguna where email = %s and password = %s",[id, password])
        rows = namedtuplefetchall(cursor)
        return rows

def get_organizer(em):
    with connection.cursor() as cursor:
        cursor.execute("select email from event.organizer where email = %s",[em])
        rows = namedtuplefetchall(cursor)
        return rows

def register_new_pengujung(em, nd, nb):
    with connection.cursor() as cursor:
        cursor.execute("insert into event.pengunjung(email, nama_depan, nama_belakang) values (%s, %s, %s)",[em,nd,nb])
        return cursor

def register_new_individu(em, ktp, nd, nb):
    with connection.cursor() as cursor:
        cursor.execute("insert into event.individu(email, no_ktp, nama_depan, nama_belakang) values (%s, %s, %s, %s)",[em,ktp,nd,nb])
        return cursor

def register_new_perusahaan(em, nm):
     with connection.cursor() as cursor:
         cursor.execute("insert into event.perusahaan(email, nama) values (%s, %s)",[em ,nm])
         return cursor