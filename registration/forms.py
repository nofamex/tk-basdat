from django import forms

class VisitorForm(forms.Form):
    email = forms.EmailField(
        label='Email'
    )
    password = forms.CharField(
        label='Password',
        max_length=64,
        required=True,
        widget=forms.PasswordInput()
    )
    nama = forms.CharField(
        label='Nama',
        max_length=100,
    )
    alamat = forms.CharField(
        label='Alamat',
        max_length=256,
    )
    

class OrganizerForm(forms.Form):
    tipe_organizer = forms.CharField(
        label='Tipe Organizer',
        widget=forms.RadioSelect(
            choices=[
                ('Individu', 'Individu'),
                ('Perusahaan', 'Perusahaan'),
            ]
        )
    )
    email = forms.EmailField(
        label='Email'
    )
    password = forms.CharField(
        label='Password',
        max_length=64,
        required=True,
        widget=forms.PasswordInput()
    )
    npwp_or_ktp = forms.CharField(
        label='NPWP/No KTP',
        max_length=20,
    )
    nama = forms.CharField(
        label='Nama',
        max_length=100,
    )