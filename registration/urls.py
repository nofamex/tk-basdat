from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.main_menu_register, name = 'main_menu_register'),
    path("pengunjung/", views.register_pengunjung, name = 'register_pengunjung'),
    path("organizer/", views.register_organizer, name = 'register_organizer'),
]
