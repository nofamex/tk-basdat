from django.shortcuts import render, reverse, redirect
from .forms import VisitorForm, OrganizerForm
from .db import get_user, register_new_pengujung, get_organizer, register_new_individu, register_new_perusahaan


def main_menu_register(request):
    context = {
        'register_pengunjung' : reverse('register_pengunjung'),
        'register_organizer' : reverse('register_organizer'),
    }

    return render(request, 'auth/register.html', context)


def register_pengunjung(request):
    if request.method == "POST":
        form = VisitorForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            nama = form.cleaned_data['nama'].split(" ")
            nama_depan = nama[0]
            nama_belakang = nama[1]
            
            user = get_user(email, password)

            if user == []:
                context = {
                    'form': form,
                    "false" : True
                }
                return render(request, 'auth/register_pengunjung.html', context)
            
            try:
                newuser = register_new_pengujung(email, nama_depan, nama_belakang)
            except Exception:
                context = {
                    'form': form,
                    'false': True
                }
                return render(request, 'auth/register_pengunjung.html', context)
            
            return redirect('/login/')


    else:
        form = VisitorForm(request.POST or None)
        context = {
            'form': form,
        }
        return render(request, 'auth/register_pengunjung.html', context)


def register_organizer(request):
    if request.method == "POST":
        form = OrganizerForm(request.POST)
        if form.is_valid():
            tipe = form.cleaned_data['tipe_organizer']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            idfier = form.cleaned_data['npwp_or_ktp']
            nama = form.cleaned_data['nama'].split(" ")
            nama_depan = nama[0]
            nama_belakang = nama[1]

            user = get_user(email, password)
            organizer = get_organizer(email)

            if user == []:
                context = {
                    'form': form,
                    "false" : True
                }
                return render(request, 'auth/register_organizer.html', context)

            if organizer == []:
                context = {
                    'form': form,
                    "false" : True
                }
                return render(request, 'auth/register_organizer.html', context)    
            
            try:
                if tipe == 'Individu':
                    individu = register_new_individu(email, idfier, nama_depan, nama_belakang)
                else:
                    perusahaan = register_new_perusahaan(email, nama_depan)
            except Exception:
                context = {
                    'form': form,
                    'false': True
                }
                return render(request, 'auth/register_organizer.html', context)
                
            return redirect('/login/')
            

    else:
        form = OrganizerForm(request.POST or None)
        context = {
            'form': form,
        }
        return render(request, 'auth/register_organizer.html', context)