from django.shortcuts import render

# Create your views here.
def payticket_landing(request):
    return render(request, 'payticket/payticket.html')

def change_ticket(request):
    return render(request, 'payticket/update.html')

def delete_transaction(request):
    return render(request, 'payticket/delete.html')