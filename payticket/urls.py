from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.payticket_landing, name = 'payticket_landing'),
    path("update", views.change_ticket, name="change_ticket"),
    path("delete", views.delete_transaction, name="delete")
]
