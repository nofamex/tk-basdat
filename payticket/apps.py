from django.apps import AppConfig


class PayticketConfig(AppConfig):
    name = 'payticket'
